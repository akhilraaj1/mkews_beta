#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    MagikEye Serializer Definition's For Datasets and Jobs
"""
__author__ = "Jigar Patel" 
__e_mail__ = "jigar@magikeye.com" 
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ----------------------------------------------------------------------------------------------------------------------

from rest_framework import serializers
from dbapi.models import Datasetsdata, Jobsdata

class DatasetsSerializer(serializers.ModelSerializer):
    class Meta:
        """
        :Author:  Jigar Patel
        :Description: Model serializer for datasets for a given user
        :Model Fields: ‘ID’, ‘USER’, ‘DATE_CREATED’, ‘COMMENT’
        """
        model = Datasetsdata
        fields = '__all__' 

# ----------------------------------------------------------------------------------------------------------------------

class JobsSerializer(serializers.ModelSerializer):
    class Meta:
        """
        :Author:  Jigar Patel
        :Description: Model serializer for jobs for a given user
        :Model Fields: ‘ID’, ‘USER’, ‘DATASET_ID’, ‘DATE_CREATED’, ‘TYPE’, ‘PROCESS_ID’
        """
        model = Jobsdata
        fields = '__all__' 
