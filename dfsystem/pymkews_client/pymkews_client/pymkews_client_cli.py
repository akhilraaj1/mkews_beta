#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
PYMkEWS client
"""

__author__ = "Jigar patel"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
__version__ = '1.0.0'

# ------------------------------------------------------------------------------

import argparse
from pymkews_client.client import Client

# ------------------------------------------------------------------------------

# Defining argument parser
parser = argparse.ArgumentParser(description="Pymkews client package.")
parser.add_argument('-u','--username',type=str, metavar='',help="Username")
parser.add_argument('-p','--password',type=str, metavar='',help="Password")

# Set Log level
level = parser.add_mutually_exclusive_group()
level.add_argument('-q','--quiet', action='store_true')
level.add_argument('-v','--verbose', action='store_true')

# List Dataset
parser.add_argument('-ld','--list_datasets',action='store_true',help="List datasets")

# Create Dataset
parser.add_argument('-cd','--create_dataset',action='store_true',help="Create Dataset")
parser.add_argument('-dc','--dataset_comment',type=str, metavar='',help="Add comment to dataset")
parser.add_argument('-df','--dataset_file',type=str, metavar='',help="Add file to dataset")

# Update dataset - File / Comment
parser.add_argument('-uc','--update_comment',type=str, metavar='',help="Update Comment")
parser.add_argument('-uf','--update_file',type=str, metavar='',help="Update File")
parser.add_argument('-did','--dataset_id',type=int, metavar='',help="Dataset id")

# Delete dataset
parser.add_argument('-dd','--delete_dataset',type=int, metavar='',help="Dataset id for dataset to be deleted")

# List files in dataset
parser.add_argument('-ldf','--list_datasetfiles',type=int, metavar='',help="")

# Get file from dataset
parser.add_argument('-gdf','--get_dataset_file',type=str, metavar='',help="")
parser.add_argument('-fsp','--file_save_path',type=str, metavar='',help="")

# List Jobs
parser.add_argument('-lj','--list_jobs',action='store_true',help="List Jobs")

# Create Job
parser.add_argument('-cj','--create_job', type=str, metavar='',help="Create job")

# Delete Job
parser.add_argument('-dj','--delete_job', type=int, metavar='',help="Delete job")

# Get Job files
parser.add_argument('-gjf','--get_job_files', action='store_true',help="Get job files")
parser.add_argument('-jid','--job_id', type=str, metavar='',help="Provide job id")

# Query Job
parser.add_argument('-qj','--query_job', type=int, metavar='',help="Provide jobs json file path for creating job")

args = parser.parse_args()

# if __name__ == "__main__":
def main():
    cli = Client(username=args.username,password=args.password)

    # Set Level
    if(args.quiet):
        cli.set_log_level(1)
    elif(args.verbose):
        cli.set_log_level(0)

    # List datasets
    if(args.list_datasets):
        datasets = cli.list_datasets()
        for data in datasets:
            print(data)

    # Create dataset
    if(args.create_dataset):
        dataset_id = cli.create_dataset(comment=args.dataset_comment,file_path=args.dataset_file)
        print(dataset_id)

    # Update comment
    if(args.update_comment):
        cli.update_comment(dataset_id=args.dataset_id,comment=args.update_comment)
    
    # Update file
    if(args.update_file):
        cli.update_file(dataset_id=args.dataset_id,file_path=args.update_file)

    # Delete dataset
    if(args.delete_dataset):
        cli.delete_dataset(dataset_id=args.delete_dataset)

    # List dataset files
    if(args.list_datasetfiles):
        dataset_files = cli.list_dataset_files(args.list_datasetfiles)
        print(dataset_files)

    # Get dataset file
    if(args.get_dataset_file):
        if(not args.file_save_path):
            print("Please provide file save path")
        elif(not args.dataset_id):
            print("Please provide dataset ID")
        else:
            cli.get_file_from_dataset(dataset_id = args.dataset_id, file_name = args.get_dataset_file, file_save_path = args.file_save_path)

    # List jobs
    if(args.list_jobs):
        jobs = cli.list_jobs()
        for job in jobs:
            print(job)

    # Create job
    if(args.create_job):
        job_id = cli.create_job(json_file_path=args.create_job)
        print(job_id)

    # Delete job
    if(args.delete_job):
        cli.delete_job(job_id=args.delete_job)

    # Download jobfiles
    if(args.get_job_files):
        if(not args.job_id):
            print("Please provide job ID")
        elif(not args.file_save_path):
            print("Please provide file save path")
        else:
            cli.get_files_from_job(job_id = args.job_id, file_save_path = args.file_save_path)

    # Query Jobs
    if(args.query_job):
        status = cli.query_job(job_id=args.query_job)
        print(status)
