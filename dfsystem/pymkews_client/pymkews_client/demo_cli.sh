#!/usr/bin/env bash

# Author : Jigar Patel
# Copyright (c) 2020, Magik-Eye Inc.

# Log level
# -v -q
# verbose
pymkews_client_cli -v
# quiet
pymkews_client_cli -u test_user_1 -q

# Datasets utility
# 1. List datasets 
# DTSS=`pymkews_client_cli -ld`
# echo "List of datasets"
# echo "$DTSS"

# 2. Create dataset
# DTSID=`./pymkews_client_cli.py -cd`
# DTSID=`./pymkews_client_cli.py --create_dataset --dataset_comment comment1`
# echo "Created dataset, $DTSID"

# 3. Update dataset - file/comment
# `./pymkews_client_cli.py -uc comment2 -did $DTSID`
# `./pymkews_client_cli.py -uf "data/magik_eye1.png" -did $DTSID -v`

# 4. Delete dataset
# `./pymkews_client_cli.py -dd 120 -v`

# 5. List files in dataset
# FILES=`./pymkews_client_cli.py -ldf 121`
# echo "List of files : "
# echo "$FILES"

# 6. get dataset files
# `./pymkews_client_cli.py -v -gdf 'magik_eye3.png' -fsp "/home/jigar/Desktop" -did 121`

# Jobs utility
# 1. List jobs
# JOBS=`./pymkews_client_cli.py -lj`
# echo "List of jobs"
# echo "$JOBS"

# 2. Create job
# JOBID=`./pymkews_client_cli.py -cj "data/job1.json"`
# echo "Jobs created with job id : $JOBID"

# 3. Delete job
# `./pymkews_client_cli.py -dj 149 -v`

# 4. Get job files
# `./pymkews_client_cli.py -v -gjf -jid 147 -fsp '/home/jigar/Desktop'`

# 5. Query jobs
# STATUS=`./pymkews_client_cli.py -qj 145 -v`
# echo "$STATUS"
