Examples
=============

Installation:
*************
As the package has not been published on PyPi, it CANNOT be installed using pip.

However, Magik-eye will provide a wheel file : ``mkews_client_package.whl``. The package can be pip installed using this wheel file.

Several dependencies required by the package will also be installed while installing the package.

Usage : Create datasets, create jobs and query jobs statuses
************************************************************
.. code-block:: python

    """This example demonstrates simple usage of the client package.
    
    - All the methods specific to datasets have been explained initially.
    
    - Methods specific to jobs have been explained later.

    """
    """
    PYMkEWS client
    """

    __author__ = "Jigar patel"
    __copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
    __version__ = '1.0.0'

    # ------------------------------------------------------------------------------

    from client import pyClient

    # ------------------------------------------------------------------------------
    # Normal User
    # Login
    cli = pyClient("jigar")

    # Datasets calls
    print("Datasets API: ")

    print("1. List datasets")
    print(cli.list_datasets.__doc__)
    # data = cli.list_datasets()
    # for row in data:
    #     print(row)

    print("2. Create dataset")
    # dataset_id = cli.create_dataset(comment="Comment added while creating dataset",file_path='/media/jigar/A4F2A156F2A12D8C/MAGIK_EYE/magik_eye_db/magikeye-integration-beta/mkews/pymkews_client/pymkews_client/small-1.png')

    # for i in range(10):
        # cli.create_dataset()

    print("3. Update file")
    # dataset_id = cli.update_file(dataset_id='103',file_path='/media/jigar/A4F2A156F2A12D8C/MAGIK_EYE/magik_eye_db/magikeye-integration-beta/mkews/pymkews_client/pymkews_client/small-1.png')

    print("3. Update comment")
    # for i in range(99,104):
    #     dataset_id = cli.update_comment(dataset_id=i,comment="Comment updated for dataset:"+str(i))

    print("4. Delete dataset")
    # for i in range(100,105):
    #     dataset_id = cli.delete_dataset(dataset_id=i)

    print("6. List files in dataset")
    # dataset_files = cli.list_dataset_files(dataset_id=104)
    # print("dataset_files : ",dataset_files)

    print("7. Get file from dataset")
    # response = cli.get_file_from_dataset(dataset_id = 106,file_name = 'small-1.png',file_save_path='/media/jigar/A4F2A156F2A12D8C/MAGIK_EYE/magik_eye_db/magikeye_intergation_new/magikeye-integration-beta/mkews/pymkews_client/pymkews_client')
    # for file_itr in dataset_files:
    #     print(file_itr)
    #     response = cli.get_file_from_dataset(dataset_id = 104,file_name = file_itr,file_save_path='/media/jigar/A4F2A156F2A12D8C/MAGIK_EYE/magik_eye_db/magikeye_intergation_new/magikeye-integration-beta/mkews/pymkews_client/pymkews_client')

    # register

    # Jobs calls
    print()
    print("Jobs API: ")

    print("1. List Jobs")
    # data = cli.list_jobs()
    # for row in data:
    #     print(row)

    print("2. Create Job")
    print(cli.create_job.__doc__)
    # for i in range(5):
    #     cli.create_job(json_file_path="/media/jigar/A4F2A156F2A12D8C/MAGIK_EYE/magik_eye_db/magikeye_intergation_new/magikeye-integration-beta/mkews/pymkews_client/pymkews_client/jobs.json")

    print("3. Delete Jobs")
    # data = cli.delete_job(job_id=89)

    # TODO
    print("4. Add data to Jobs")
    # cli.add_files()

    print("5. Query Jobs")
    # cli.query_job(job_id=88)
