from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404

from django.contrib import messages
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from .forms import UserRegisterationForm
from django.contrib.auth.decorators import login_required

import json, requests, ast, mimetypes
from connecto import views as app_view
from connect import settings

secret_key = settings.SECRET_KEY


# Application views here.
def register(request):
    if request.method == 'POST':
        form = UserRegisterationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Your account has been created! Please log in!')
            return redirect('login')
    else:
        form = UserRegisterationForm()
    return render(request, 'register.html', {'form': form})

@login_required()
def profile(request):
    return render(request, 'profile.html')


def create_datasets(request):
    if request.method == 'POST':
        msg = None
        username = request.user.username
        user = User.objects.get(username=username)
        secret_token = Token.objects.get(user=user).key
        # FS call :: getFSUsers
        payload = {"secret_key": secret_key}
        payload = json.dumps(payload)
        response = requests.post(app_view.DB_HOST + app_view.getFSUsers_api, data=payload)
        fs_users = (response.json())["users"]
        if username not in fs_users:
            # FS call :: createFSUser
            payload = {"username": username, "secret_key": secret_key}
            payload = json.dumps(payload)
            requests.post(app_view.DB_HOST + app_view.createFSUser_api, data=payload)

        # DB call :: createDataSet
        payload = {"username": username, "secret_token": secret_token}
        payload = json.dumps(payload)
        response = requests.post(app_view.DB_HOST + app_view.create_data_set_api, data=payload)
        data_set_id = int(response.json()['data_set_id'])

        # FS call :: createFSDataset
        payload = {"username": username, "data_set_id": data_set_id, "secret_token": secret_token}
        payload = json.dumps(payload)
        requests.post(app_view.DB_HOST + app_view.createFSDataset_api, data=payload)

        if 'dataset' in request.FILES:
            # Create dataset along with the Datafile!
            file_uploaded = request.FILES['dataset']
            file_name = file_uploaded.name

            # FS call :: addFileToFSDataset
            payload = {"username": username, "data_set_id": data_set_id, "filename": file_name,
                       "secret_token": secret_token}
            requests.post(app_view.DB_HOST + app_view.addFileToFSDataset_api, data=payload,
                          files={'file': file_uploaded})

            msg = f"Your Dataset-{data_set_id} has been Created with file {file_uploaded.name} Successfully!"

        if not msg: msg = f"Your Dataset-{data_set_id} has been Created Successfully!"
        messages.success(request, msg)

    return redirect('home')

def upload_files_for_DS(request):
    if request.method == 'POST':
        username = request.user.username
        user = User.objects.get(username=username)
        secret_token = Token.objects.get(user=user).key

        file_uploaded = request.FILES['datasetfile']
        data_set_id = request.POST['data_set_id']
        file_name = file_uploaded.name

        payload = {"username": username, "data_set_id": data_set_id, "filename": file_name, "secret_token": secret_token}
        requests.post(app_view.DB_HOST + app_view.addFileToFSDataset_api, data=payload, files={'file': file_uploaded})

        messages.success(request, f'Your File {file_uploaded.name} has been uploaded to the Dataset-{data_set_id} Successfully!')

    return redirect('home')

def upload_files_for_DS_from_DSD(request):
    if request.method == 'POST':
        username = request.user.username
        user = User.objects.get(username=username)
        secret_token = Token.objects.get(user=user).key

        file_uploaded = request.FILES['datasetfile']
        data_set_id = request.POST['data_set_id']
        file_name = file_uploaded.name

        payload = {"username": username, "data_set_id": data_set_id, "filename": file_name, "secret_token": secret_token}
        requests.post(app_view.DB_HOST + app_view.addFileToFSDataset_api, data=payload, files={'file': file_uploaded})

        messages.success(request, f'Your File {file_uploaded.name} has been uploaded to the Dataset-{data_set_id} Successfully!')

    return redirect('dataSetDetails', data_set_id=data_set_id)

def update_comment_for_DS(request):
    if request.method == 'POST':
        username = request.user.username
        user = User.objects.get(username=username)
        secret_token = Token.objects.get(user=user).key

        comment = request.POST['datasetcomment']
        data_set_id = request.POST['data_set_id']
        payload = {'data_set_id': data_set_id, 'comment': comment, "secret_token": secret_token}
        payload = json.dumps(payload)

        # DB call :: updateDataSet
        response = requests.post(app_view.DB_HOST + app_view.update_data_set_api, data=payload)
        if response.status_code == 200:
            messages.success(request, f'Your comment has been updated to the Dataset-{data_set_id} Successfully!')
        else:
            messages.warning(request, f"Your comment couldn't be updated to the Dataset-{data_set_id}, Please try again!")
    return redirect('home')

def remove_dataset(request):
    if request.method == 'POST':
        username = request.user.username
        user = User.objects.get(username=username)
        secret_token = Token.objects.get(user=user).key

        data_set_id = request.POST['data_set_id']
        payload = {'data_set_id': data_set_id, "secret_token": secret_token}
        payload = json.dumps(payload)

        # DB call :: removeDataSet
        response = requests.post(app_view.DB_HOST + app_view.remove_data_set_api, data=payload)
        if response.status_code == 200:
            payload = {'username': username, 'data_set_id': data_set_id, "secret_token": secret_token}
            payload = json.dumps(payload)
            # FS call :: removeFSDataset
            response = requests.post(app_view.DB_HOST + app_view.removeFSDataset_api, data=payload)
            if response.status_code == 200:
                messages.success(request, f'Your Dataset-{data_set_id} has been removed\
                                            from both DB and FS Successfully')
            else:
                messages.warning(request, f"Your Dataset-{data_set_id} has been removed\
                                            from DB Successfully, but still exists in filesystem!")

        else:
            messages.warning(request, f"Your Dataset-{data_set_id} couldn't be removed , Please try again!")

    return redirect('home')


def create_job(request):
    if request.method == 'POST':
        username = request.user.username
        user = User.objects.get(username=username)
        secret_token = Token.objects.get(user=user).key

        file_uploaded = request.FILES['jobfile']

        # Job File Validation - 1
        file_extension_validated_successfully = True
        acceptable_file_formats = ['json']

        file_extension = (file_uploaded.name.split('.'))[-1]
        if file_extension.lower().strip() not in acceptable_file_formats:
            file_extension_validated_successfully = False
            msg = f"File extension - '{file_extension.lower().strip()}' is not acceptable file formats: {acceptable_file_formats}"
            messages.warning(request, f"Cannot validate job JSON: {msg}")

        if file_extension_validated_successfully:
            json_file_content = file_uploaded.read()
            job_file_as_dict = ast.literal_eval(json_file_content.decode('utf-8'))
            j_type = job_file_as_dict['type']

            # DB call :: createJob
            if j_type != "sleep":
                dataset_id = job_file_as_dict['dataset_id']
                payload = {'username': username, 'jobs_type': j_type, 'jobs_process': 'PROCESS_UNKNOWN',
                           'dataset_id': dataset_id, "secret_token": secret_token}
            else:
                payload = {'username': username, 'jobs_type': j_type, 'jobs_process': 'PROCESS_UNKNOWN',
                           'dataset_id': "NA", "secret_token": secret_token}
            payload = json.dumps(payload)
            # payload should needs to be updated with the process!

            response = requests.post(app_view.DB_HOST + app_view.create_job_api, data=payload)
            job_id = int(response.json()['job_id'])

            # FS Call: createFSJob
            payload = {"username": username, "job_id": job_id, "job_file_as_dict": job_file_as_dict,
                       "secret_token": secret_token}
            payload = json.dumps(payload)
            job_executor_response = requests.post(app_view.DB_HOST + app_view.createFSJob_api, data=payload)
            job_executor_response_json = job_executor_response.json()

            if job_executor_response_json["type"] == "success":
                pstatus = job_executor_response_json["pstatus"]

                # DB call :: updateJob
                payload = {'job_id': job_id,  'jobs_process': pstatus, "secret_token": secret_token}
                payload = json.dumps(payload)
                response = requests.post(app_view.DB_HOST + app_view.update_job_api, data=payload)

                if response.status_code == 200:
                    messages.success(request, f'Your Job File {file_uploaded.name} has been uploaded to the Job-{job_id} Successfully!')
                else:
                    messages.warning(request, f"Your Job File couldn't be uploaded, Please try again!")

            elif "validationError" in job_executor_response_json:
                # DB call :: removeJob
                # Job entry has been already initiated in the DB in  order to get the job id.
                # Now that entry is reverted here, since the job json is invalid.
                payload = {'job_id': job_id, "secret_token": secret_token}
                payload = json.dumps(payload)

                response = requests.post(app_view.DB_HOST + app_view.remove_job_api, data=payload)
                messages.warning(request, job_executor_response_json["message"])

    return redirect('jobs')


def refresh_job(request, job_id, process):
    username = request.user.username
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key

    if 'running' in process.lower():
        payload = {"username": username, "job_id": job_id, "secret_token": secret_token}
        payload = json.dumps(payload)
        job_executor_response = requests.post(app_view.DB_HOST + app_view.jobExecutorQueryProcess_api, data=payload)
        job_executor_response_json = job_executor_response.json()
        process_status = job_executor_response_json["process_status"]
        # DB call :: updateJob
        payload = {'job_id': job_id, 'jobs_process': process_status, "secret_token": secret_token}
        payload = json.dumps(payload)
        response = requests.post(app_view.DB_HOST + app_view.update_job_api, data=payload)

    return redirect('jobs')


def remove_job(request):
    if request.method == 'POST':
        username = request.user.username
        user = User.objects.get(username=username)
        secret_token = Token.objects.get(user=user).key

        job_id = request.POST['job_id']

        # DB call :: removeJob
        payload = {'job_id': job_id, "secret_token": secret_token}
        payload = json.dumps(payload)
        response = requests.post(app_view.DB_HOST + app_view.remove_job_api, data=payload)

        if response.status_code == 200:
            messages.success(request, f'Your Job-{job_id} has been removed Successfully!')
            # FS call :: removeFSJob
            payload = {"username": username, "job_id": job_id, "secret_token": secret_token}
            payload = json.dumps(payload)
            requests.post(app_view.DB_HOST + app_view.removeFSJob_api, data=payload)
        else:
            messages.warning(request, f"Your Job-{job_id}  couldn't be removed, Please try again!")

    return redirect('jobs')

def get_data_sets_from_fs(username):
    """
    :param username:
    :return: Dictionary -> {data_set_id: ("dataset_url", [dataset_files])}
    """
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key

    # FS call :: getFSDatasets
    payload = {"username": username, "secret_token": secret_token}
    payload = json.dumps(payload)
    response = requests.post(app_view.DB_HOST + app_view.getFSDatasets_api, data=payload)
    if response.status_code == 200:
        data_sets = (response.json())["datasets"]
    else:
        data_sets = []
        messages.warning("Unable to fetch the datasets from Filesystem")
    return data_sets


def view_dataset_file(request, data_set_id, file_name):
    #Note: Users should be allowed to view, only the files which are uploaded by them.
    username = request.user.username
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key
    try:
        # FS call :: getFileFromFSDataset
        payload = {"username": username, "data_set_id": data_set_id, "file_name": file_name,
                   "secret_token": secret_token}
        payload = json.dumps(payload)
        # File Response[File as IO object] will be received.
        dataset_file_content = requests.post(app_view.DB_HOST + app_view.getFileFromFSDataset_api, data=payload)
        response = HttpResponse(dataset_file_content, content_type=mimetypes.guess_type(file_name)[0])
        response['Content-Disposition'] = "inline; filename=" + file_name
        return response
    except Exception:
        raise Http404


def download_dataset_file(request, data_set_id, file_name):
    # Note: Users should be allowed to download, only the files which are uploaded by them.
    username = request.user.username
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key
    try:
        # FS call :: getFileFromFSDataset
        payload = {"username": username, "data_set_id": data_set_id, "file_name": file_name,
                   "secret_token": secret_token}
        payload = json.dumps(payload)
        # File Response[File as IO object] will be received.
        dataset_file_content = requests.post(app_view.DB_HOST + app_view.getFileFromFSDataset_api, data=payload)
        response = HttpResponse(dataset_file_content, content_type=mimetypes.guess_type(file_name)[0])
        response['Content-Disposition'] = "attachment; filename=" + file_name
        return response
    except Exception:
        raise Http404


def get_jobs_from_fs(username):
    """
    :param username:
    :return:  Dictionary -> {job_id: ("job_url", [job_files])}
    """
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key

    # FS call :: getFSJobs
    payload = {"username": username, "secret_token": secret_token}
    payload = json.dumps(payload)
    response = requests.post(app_view.DB_HOST + app_view.getFSJobs_api, data=payload)
    if response.status_code == 200:
        jobs = (response.json())["jobs"]
    else:
        jobs = []
        messages.warning("Unable to fetch the jobs from Filesystem")
    return jobs

def view_job_file(request, job_id, file_name):
    # Note: Users should be allowed to view, only the files which are uploaded by them.
    username = request.user.username
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key
    try:
        payload = {"username": username, "job_id": job_id, "file_name": file_name, "secret_token": secret_token}
        payload = json.dumps(payload)
        # File Response[File as IO object] will be received.
        job_file_content = requests.post(app_view.DB_HOST + app_view.getFileFromFSJob_api, data=payload)
        response = HttpResponse(job_file_content, content_type=mimetypes.guess_type(file_name)[0])
        response['Content-Disposition'] = "inline; filename=" + file_name
        return response
    except Exception:
        raise Http404

def download_job_file(request, job_id, file_name):
    # Note: Users should be allowed to download, only the files which are uploaded by them.
    username = request.user.username
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key
    try:
        payload = {"username": username, "job_id": job_id, "file_name": file_name, "secret_token": secret_token}
        payload = json.dumps(payload)
        # File Response[File as IO object] will be received.
        job_file_content = requests.post(app_view.DB_HOST + app_view.getFileFromFSJob_api, data=payload)
        response = HttpResponse(job_file_content, content_type=mimetypes.guess_type(file_name)[0])
        response['Content-Disposition'] = "attachment; filename=" + f'job_{job_id}_{file_name}'
        return response
    except Exception:
        raise Http404

