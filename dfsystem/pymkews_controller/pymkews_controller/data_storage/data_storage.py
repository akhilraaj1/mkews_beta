# -*- coding: utf-8 -*-

"""
MkEWS Data Storage
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
# ------------------------------------------------------------------------------

from abc import ABC, abstractmethod
from typing import List, IO


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class DataStorage(ABC):

    DEFAULT_DATASET_ID = 0

    # Users -------------------------------------------------------------------

    @abstractmethod
    def create_user(self, user_name: str) -> str:
        pass

    @abstractmethod
    def delete_user(self, user_name: str) -> None:
        pass

    @abstractmethod
    def list_users(self) -> List[str]:
        pass

    # Datasets ----------------------------------------------------------------

    @abstractmethod
    def create_dataset(self, user_name: str, dataset_id: int) -> str:
        pass

    @abstractmethod
    def get_dataset_url(self, user_name: str, dataset_id: int, exists: bool = False) -> str:
        pass

    @abstractmethod
    def add_file_to_dataset(self, user_name: str, dataset_id: int, file_path: str) -> None:
        pass

    @abstractmethod
    def get_file_from_dataset(self, user_name: str, dataset_id: int, file_name: str) -> IO:
        pass

    @abstractmethod
    def list_dataset_files(self, user_name: str, dataset_id: int) -> List[str]:
        pass

    @abstractmethod
    def delete_dataset(self, user_name: str, dataset_id: int):
        pass

    @abstractmethod
    def list_datasets(self, user_name: str) -> List[int]:
        pass

    # Jobs --------------------------------------------------------------------

    @abstractmethod
    def create_job(self, user_name: str, job_id: int) -> str:
        pass

    @abstractmethod
    def get_job_url(self, user_name: str, job_id: int) -> str:
        pass

    @abstractmethod
    def add_data_to_job(self, user_name: str, job_id: int, file_name: str, file_data: bytes) -> None:
        pass

    @abstractmethod
    def get_file_from_job(self, user_name: str, job_id: int, file_name: str) -> IO:
        pass

    @abstractmethod
    def list_job_files(self, user_name: str, job_id: int) -> List[str]:
        pass

    @abstractmethod
    def delete_job(self, user_name: str, job_id: int):
        pass

    @abstractmethod
    def list_jobs(self, user_name: str):
        pass
