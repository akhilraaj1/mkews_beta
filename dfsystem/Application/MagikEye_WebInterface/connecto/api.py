#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    MagikEye APi's For Handling Authentication and Tokens
"""
__author__ = "karthik"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
# ----------------------------------------------------------------------------------------------------------------------
import json

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from rest_framework.authtoken.models import Token

"""
 SECRET_KEY is same for both the Browser-Interface App and Backend App.
 SECRET_KEY enables a secure communication between Browser-Interface App and Backend App.
 Tokens are transferred to Backend App through getsecretToken API. using this SECRET_KEY.
"""
from connect import settings
APP_SECRET_KEY = settings.SECRET_KEY

# API's for Authentication:

@api_view(["POST"])
def getsecretToken(payload):
    data = json.loads(payload.body)
    try:
        secret_key = data["secret_key"]
        if secret_key == APP_SECRET_KEY:
            try:
                secret_tokens = []
                user_objects = User.objects.all()
                for user_object in user_objects:
                    token_dict = {}
                    username = user_object.username
                    token = Token.objects.get(user=user_object).key
                    token_dict["username"] = username
                    token_dict["token"] = token
                    secret_tokens.append(token_dict)
                response = {"secret_tokens": secret_tokens, "type": "success"}

                return Response(response, status.HTTP_200_OK)
            except Exception as err_message:
                response = {"message": str(err_message), "type": "error"}
                return Response(response, status.HTTP_400_BAD_REQUEST)
        else:
            response = {"message": "Unauthorized. Invalid secret key!", "type": "error"}
            return Response(response, status.HTTP_401_UNAUTHORIZED)

    except Exception as err_message:
        if "secret_key" not in data:
            err_message = "Please provide the mandatory Fields ['secret_key'] in Payload."
        response = {"message": str(err_message), "type": "error"}
        return Response(response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def login(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        password_provided = data["password"]
        try:
            user = User.objects.get(username=username)
            actual_password = user.password
            password_valid = check_password(password_provided, actual_password)
            if password_valid:
                secret_token = Token.objects.get(user=user).key
                response = {"message": "You're authenticated and logged in successfully", "secret_token": secret_token,
                            "type": "success"}
                return Response(response, status.HTTP_200_OK)
            else:
                err_message = f"Invalid Password, Please try again!"
                response = {"message": err_message, "type": "error"}
                return Response(response, status.HTTP_400_BAD_REQUEST)

        except Exception as err_message:
            if 'User matching query does not exist' in str(err_message):
                err_message = f"User '{username}' is not found"
            response = {"message": str(err_message), "type": "error"}
            return Response(response, status.HTTP_400_BAD_REQUEST)

    except Exception as err_message:
        if ("username" not in data) or ("password" not in data):
            err_message = "Please provide the mandatory Fields ['username', 'password'] in Payload."
        response = {"message": str(err_message), "type": "error"}
        return Response(response, status.HTTP_400_BAD_REQUEST)
