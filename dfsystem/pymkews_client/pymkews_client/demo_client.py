# -*- coding: utf-8 -*-

"""
PYMkEWS client
"""

__author__ = "Jigar patel"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
__version__ = '1.0.0'

# ------------------------------------------------------------------------------

from client import Client

# ------------------------------------------------------------------------------

# cli = Client()
# cli = Client(username="test_suser_1")
# cli = Client(password="Pythonr36*")
# cli = Client()

# Set log level 
# 0 - INFO , 1 - ERROR
# cli.set_log_level(1)
# cli.set_log_level(0)

# Datasets calls
print("Datasets API: ")

print("1. List datasets")
# print(cli.list_datasets.__doc__)
# data = cli.list_datasets()
# for row in data:
#     print("datasets ",row)

print("2. Create dataset")
# dataset_id = cli.create_dataset()
# dataset_id = cli.create_dataset(comment="Comment added while creating dataset",file_path='data/magik_eye1.png')
# dataset_id = cli.create_dataset(comment="Comment added while creating dataset")
# dataset_id = cli.create_dataset(file_path='data/magik_eye1.png')

print("3. Update file")
# dataset_id = cli.update_file(dataset_id=120,file_path='data/magik_eye1.png')
# dataset_id = cli.update_file(dataset_id=121,file_path='data/magik_eye3.png')

print("3. Update comment")
# status = cli.update_comment(dataset_id=115,comment="Comment updated")

print("4. Delete dataset")
# status = cli.delete_dataset(dataset_id=118)

print("6. List files in dataset")
# dataset_files = cli.list_dataset_files(dataset_id=114)

print("7. Get file from dataset")
# status = cli.get_file_from_dataset(dataset_id = 119,file_name = 'magik_eye1.png',file_save_path = '/home/jigar/Desktop')

# Jobs calls
print()
print("Jobs API: ")

print("1. List Jobs")
# data = cli.list_jobs()
# for row in data:
#     print("jobs ",row)

print("2. Create Job")
# cli.create_job(json_file_path="data/job1.json")

print("3. Delete Jobs")
# cli.delete_job(job_id=140)

print("4. Get Job files ")
# cli.get_files_from_job(job_id = 139, file_save_path = '/home/jigar/Desktop')

print("5. Query jobs ")
# status = cli.query_job(job_id=112)
# print(status)
