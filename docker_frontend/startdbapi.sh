#!/bin/bash

set -e

cd /mkews_web/mkews/Application/MagikEye_BackEnd/
sleep 2
PYTHONPATH=$PYTHONPATH:/mkews_web:/mkews_web/mkews/ gunicorn magikeye.wsgi:application --workers=12 --threads=4 --worker-class=gthread --worker-connections=1024 --timeout=300 --keep-alive 5  --access-logfile '/mkews_web/dbapiapp_access_log' --bind :8002
