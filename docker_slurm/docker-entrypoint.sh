#!/bin/bash

# Author = Gunjan Sethi gunjan@magikeye.com 
# Version = 1.0

set -e
chown -R slurm:slurm /mkews_web/mkews/Application/MagikEye_BackEnd/filesystem
if [ "$HOSTNAME" = "slurmdbd" ]
then
    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start

    echo "---> Starting the Slurm Database Daemon (slurmdbd) ..."

    {
        until echo "SELECT 1" | mysql -h mysql -u slurm -ppassword 2>&1 > /dev/null
        do
            echo "-- Waiting for database to become active ..."
            sleep 2
        done
    }
    echo "-- Database is now active ..."
    chown -R slurm:slurm /data
    /etc/init.d/slurmdbd start
    sleep 60
    /usr/bin/sacctmgr --immediate add cluster name=linux
    sleep 30
    /etc/init.d/slurmdbd restart
    /bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
    
fi

if [ "$HOSTNAME" = "slurmctld" ]
then
    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start
    echo "---> Starting the sendmail service ..."
    /etc/init.d/sendmail start
    chown -R slurm:slurm /data
    echo "---> Waiting for slurmdbd to become active before starting slurmctld ..."

    until 2>/dev/null >/dev/tcp/slurmdbd/6819
    do
        echo "-- slurmdbd is not available.  Sleeping ..."
        sleep 2
    done
    echo "-- slurmdbd is now active ..."

    echo "---> Starting the Slurm Controller Daemon (slurmctld) ..."
    /etc/init.d/slurmctld start
    sleep 120
    /etc/init.d/slurmctld restart
    /bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
fi

if [ "$HOSTNAME" = "worker1" ]
then
    chown -R slurm:slurm /data
    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start

    until 2>/dev/null >/dev/tcp/slurmctld/6817
    do
        echo "-- slurmctld is not available.  Sleeping ..."
        sleep 2
    done
    echo "-- slurmctld is now active ..."

    echo "---> Starting the Slurm Node Daemon (slurmd) ..."
    /etc/init.d/slurmd start
    /bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
fi

if [ "$HOSTNAME" = "worker2" ]
then
    chown -R slurm:slurm /data
    echo "---> Starting the RSyslog service ..."
    /etc/init.d/rsyslog start
    echo "---> Starting the MUNGE Authentication service (munged) ..."
    /etc/init.d/munge start

    until 2>/dev/null >/dev/tcp/slurmctld/6817
    do
        echo "-- slurmctld is not available.  Sleeping ..."
        sleep 2
    done
    echo "-- slurmctld is now active ..."

    echo "---> Starting the Slurm Node Daemon (slurmd) ..."
    /etc/init.d/slurmd start
    /bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
fi

exec "$@"