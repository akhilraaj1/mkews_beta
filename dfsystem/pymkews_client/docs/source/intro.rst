Introduction
============

``pymkews client package`` is a Python package which aims to provide an easy and intuitive way of interacting with the Magik eye Calibration server. In essence, this package is an extension of the ``Website`` and can be used for automations. (see `here <https://github.com/IanHarvey/bluepy/>`_)

The aim here was to define a single object which would allow users to perform the various operations like creating, deleting and editing datasets and jobs. 

The example for creating datasets, creating jobs and querying the job status have been included in the ``examples`` section.

About the package
*****************

- List of methods and input/output parameters can be checked in ``pymkews client package`` section.
- Installation and Usage example can be found in ``Examples`` section.


Limitations
***********

- Creating users, Deleting users and other admin related functionalities are not a part of this package.