#!/bin/bash

# Author = Gunjan Sethi gunjan@magikeye.com 
# Version = 1.0

echo -e 'CLEANING UP OLD CONTAINERS, VOLUMES AND NETWORKS\n'

echo -e '\n>>>>>>> Looking for mkecc-cluster containers..'

if sudo docker ps -a | awk '{ print $1,$2,$NF }' | grep "mkecc*\|mysql*" ; then

    echo -e '\n>>>>>>> Stopping mkecc-cluster containers..'
    sudo docker ps -a | awk '{ print $1,$2 }' | grep "mkecc*\|mysql*" | awk '{print $1 }' | xargs -I {} docker stop {}

    echo -e '\n>>>>>>> Removing mkecc-cluster containers..'
    sudo docker ps -a | awk '{ print $1,$2 }' | grep "mkecc*\|mysql*" | awk '{print $1 }' | xargs -I {} docker rm -v {}
    
    echo -e '\n..Done!'

else
    echo -e "\nNo containers found!"
fi

echo -e '\n>>>>>>> Looking for mkecc-cluster volumes..'

if sudo docker volume ls -q| grep mkecc* ; then

    echo -e '\n>>>>>>> Removing mkecc-cluster volumes..'
    sudo docker volume rm $(docker volume ls -q| grep mkecc*)

    echo -e "\n.. Done!"

else
    echo -e "\nNo volumes found!"
fi

if sudo docker network ls | awk '{ print $1,$2}' | grep "mkecc*\|mysql*" ; then

    echo -e '\n>>>>>>> Removing mkecc-cluster volumes..'
    sudo docker network ls | awk '{ print $1,$2}' | grep "mkecc*\|mysql*" | awk '{print $1 }' | xargs -I {} docker network rm {}

    echo -e "\n.. Done!"
    
else
    echo -e "\nNo networks found!"
fi

echo -e '\n\nSYSTEM IS NOW READY FOR NEW/UPDATED CLUSTER'