import os
import glob
from setuptools import Extension, setup, Distribution

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pymkews_controller",
    version = "@PYMKEWS_WORKER_VERSION@",
    author = "Magik Eye Inc.",
    author_email = "jan@magik-eye.com",
    description = ("MkEWS Worker Package"),
    license = "Proprietary, DO NOT DISTRIBUTE",
    keywords = "mkews pymkews controller",
    url = "https://bitbucket.org/mke_rnd/mkews",
    packages=[
        'pymkews_controller',
        'pymkews_controller.job_executor',
        'pymkews_controller.data_storage',
    ],
    package_data={
      'pymkews_controller.job_executor': ['exec.slurm']
    },
    python_requires=">=3.6",
    install_requires=[
        'fs>=2.4.11',
        'psutil>=5.7.0'
    ],
    long_description=read('@CMAKE_CURRENT_SOURCE_DIR@/README.md'),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Topic :: Scientific/Engineering ",
        "License :: Other/Proprietary License",
    ]
)
