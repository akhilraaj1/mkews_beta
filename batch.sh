#building docker image and pushing to localrepository
cd docker_frontend
docker build -t localhost:5000/web .
docker push localhost:5000/web
docker build -f Dockerfile_nginx -t localhost:5000/nginx .
docker push localhost:5000/nginx
cd ..

#building docker image and pushing to localrepository
cd docker_slurm
docker build -t localhost:5000/slurm .
docker push localhost:5000/slurm
cd ..

#cleaning other pods and services
kubectl delete pods --all
kubectl delete services --all

#deploying pods and services
kubectl create -f k8s
sleep 5m

#viewing the pods
kubectl get pods
sleep 30s

#port-forwarding locally
kubectl port-forward django 8000:8000