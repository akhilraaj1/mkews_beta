# -*- coding: utf-8 -*-

"""
MkEWS Worker
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

import logging
import time
from typing import List

from .job import Job
from pymkews_worker.data_storage import DataStorage
from pymkews_worker.error import Error


# ------------------------------------------------------------------------------

class SleepV10Job(Job):

    def __init__(self):
        self.logger = logging.getLogger('pymkews.worker.sleepv10')

    # -------------------------------------------------------------------------

    def execute(self, job_json: dict, data_storage: DataStorage) -> List[str]:
        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Executing job')

        params = job_json['params']
        if 'duration' not in params:
            raise Error(f'Job type "{job_json["type"]}" requires "duration" parameter')

        time.sleep(params['duration'])

        fname = 'sleep_job_results.json'
        data_storage.add_data_to_job(fname, f'Slept for {params["duration"]} seconds'.encode('utf-8'))

        # -------------------------------------------------------------------------
        return [fname]
