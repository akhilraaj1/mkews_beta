# -*- coding: utf-8 -*-

"""
MkEWS Error
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."


# ------------------------------------------------------------------------------

class Error(RuntimeError):

    def __init__(self, message):
        super(RuntimeError, self).__init__(message)