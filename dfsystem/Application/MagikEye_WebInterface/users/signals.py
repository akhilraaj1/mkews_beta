#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    MagikEye Signals for invoking certain actions based on changes Occurring in the DataBase models.
"""

__author__ = "karthik"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ----------------------------------------------------------------------------------------------------------------------

from django.db.models.signals import post_save, post_delete
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from .models import Profile

from connect import settings
from connecto import views as app_view
from users import views as user_views
import json
import requests



@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    """
    :Author: karthik
    :Description:Creates a Profile whenever a new User instance have got created in User model.
    """
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    """
    :Author: karthik
    :Description: Created Profile of a user instance will be saved.
    """
    instance.profile.save()

@receiver(post_save, sender=User)
def genrerate_secret_token(sender, instance, created, **kwargs):
    """
    :Author: karthik
    :Description:Creates a unique secret Token whenever a new User instance have got created in User model.
    """
    if created:
        Token.objects.get_or_create(user=instance)

@receiver(post_save, sender=User)
def update_Auth_Token(sender, instance, created, **kwargs):
    """
        :Author: karthik
        :Description:Transfer the unique secret Token to DB whenever a new User instance have got created in User model.
    """
    if created:
        username = instance.username
        user_object = User.objects.get(username=username)
        token = Token.objects.get(user=user_object).key
        secret_key = settings.SECRET_KEY
        payload = {"username": username, "token": token, "secret_key": secret_key, "action": "create"}
        payload = json.dumps(payload)
        requests.post(app_view.DB_HOST + app_view.updateAuthToken_api, data=payload)


@receiver(post_save, sender=User)
def create_file_system_for_uer(sender, instance, created, **kwargs):
    """
    :Author: karthik
    :Description: Creates a filesystem whenever a new User instance have got created in User model.
    """
    if created:
        username = instance.username
        secret_key = settings.SECRET_KEY
        payload = {"username": username, "secret_key": secret_key}
        payload = json.dumps(payload)
        requests.post(app_view.DB_HOST + app_view.createFSUser_api, data=payload)

@receiver(post_delete, sender=User)
def remove_data_of_deleted_user(sender, instance, **kwargs):
    """
    :Author: karthik
    :Description: Removes all Datasets,jobs from DataBase and deletes entire filesystem, Whenever a user gets deleted.
    """
    username = instance.username
    secret_key = settings.SECRET_KEY
    payload = {'username': username, "secret_key": secret_key}
    payload = json.dumps(payload)

    # Remove Data Sets from DB
    data_sets_remove_response = requests.post(app_view.DB_HOST + app_view.remove_all_data_sets_api, data=payload)
    assert data_sets_remove_response.status_code == 200

    # Remove Jobs from DB
    jobs_remove_response = requests.post(app_view.DB_HOST + app_view.remove_all_jobs_api, data=payload)
    assert jobs_remove_response.status_code == 200

    # Remove all data for user from File System
    payload = {"username": username, "secret_key": secret_key}
    payload = json.dumps(payload)
    requests.post(app_view.DB_HOST + app_view.deleteFSUser_api, data=payload)

    #Remove AuthToken value from DB
    payload = {"username": username,  "secret_key": secret_key, "action": "delete"}
    payload = json.dumps(payload)
    requests.post(app_view.DB_HOST + app_view.updateAuthToken_api, data=payload)

