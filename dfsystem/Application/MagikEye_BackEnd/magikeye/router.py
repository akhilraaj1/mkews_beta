#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    MagikEye Router Definition's For Datasets and Jobs Rest Framework
"""
__author__ = "Jigar Patel" 
__e_mail__ = "jigar@magikeye.com" 
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ----------------------------------------------------------------------------------------------------------------------

from dbapi.viewsets import DatasetsViewset,JobsViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('datasets',DatasetsViewset)
router.register('jobs',JobsViewset)
