import os
import glob
from setuptools import Extension, setup, Distribution

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pymkews_worker",
    version = "@PYMKEWS_WORKER_VERSION@",
    author = "Magik Eye Inc.",
    author_email = "jan@magik-eye.com",
    description = ("MkEWS Worker Package"),
    license = "Proprietary, DO NOT DISTRIBUTE",
    keywords = "mkews pymkews worker",
    url = "https://bitbucket.org/mke_rnd/mkews",
    packages=[
        'pymkews_worker',
        'pymkews_worker.jobs'
    ],
    python_requires=">=3.6",
    install_requires=[
        'fs>=2.4.11',
        'pymkecc>=1.1.0'
    ],
    long_description=read('@CMAKE_CURRENT_SOURCE_DIR@/README.md'),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Topic :: Scientific/Engineering ",
        "License :: Other/Proprietary License",
    ],
    entry_points={
        "console_scripts": [
            "pymkews_worker = pymkews_worker.worker:main_func"
     ]
    }
)

