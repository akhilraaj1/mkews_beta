from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from users import views as user_views
from connect import settings
import os.path
import time
import requests
import json

DB_HOST = settings.DB_HOST

updateAuthToken_api = settings.updateAuthToken_api

#DB APi's
all_datasets = settings.all_datasets
all_jobs = settings.all_jobs
create_data_set_api = settings.create_data_set_api
update_data_set_api = settings.update_data_set_api
remove_data_set_api = settings.remove_data_set_api
remove_all_data_sets_api = settings.remove_all_data_sets_api

create_job_api = settings.create_job_api
update_job_api = settings.update_job_api
remove_job_api = settings.remove_job_api
remove_all_jobs_api = settings.remove_all_jobs_api

# FS_API'S
getFSUsers_api = settings.getFSUsers_api
createFSUser_api = settings.createFSUser_api

createFSDataset_api = settings.createFSDataset_api
getFSDatasets_api = settings.getFSDatasets_api
addFileToFSDataset_api = settings.addFileToFSDataset_api
listFSDatasetFiles_api = settings.listFSDatasetFiles_api
getFileFromFSDataset_api = settings.getFileFromFSDataset_api
removeFSDataset_api = settings.removeFSDataset_api


createFSJob_api = settings.createFSJob_api
getFSJobs_api = settings.getFSJobs_api
getFileFromFSJob_api = settings.getFileFromFSJob_api
removeFSJob_api = settings.removeFSJob_api

deleteFSUser_api = settings.deleteFSUser_api

jobExecutorQueryJob_api = settings.jobExecutorQueryJob_api

@login_required
def home(request):
    username = request.user.username
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key
    payload = {'username': username, 'secret_token': secret_token}
    payload = json.dumps(payload)
    response_dict = requests.post(DB_HOST + all_datasets, data=payload).json()
    All_Datasets_of_user = response_dict["Datasets"]

    Datasets = []
    for Dataset in All_Datasets_of_user:
        date_created = Dataset['dataset_date_created'][:10]
        time_created = Dataset['dataset_date_created'][11:19]
        data_set_files = Dataset["data_set_files"]
        Dataset.update({'dataset_date_created': date_created+'   '+time_created, 'no_of_files': len(data_set_files)})
        Datasets.append(Dataset)

    return render(request, 'home.html', {'title': 'MagikEye - Home', 'data_sets': Datasets})

@login_required
def jobs(request):
    username = request.user.username
    user = User.objects.get(username=username)
    secret_token = Token.objects.get(user=user).key
    payload = {'username': username, 'secret_token': secret_token}
    payload = json.dumps(payload)
    response_dict = requests.post(DB_HOST + all_jobs, data=payload).json()
    AllJobs_of_user = response_dict["Jobs"]
    Jobs = []
    for Job in AllJobs_of_user:
        date_created = Job['jobs_date_created'][:10]
        time = Job['jobs_date_created'][11:19]
        Job.update({'jobs_date_created': date_created+'   '+time})
        Jobs.append(Job)

    return render(request, 'jobs.html', {'title': 'MagikEye - Jobs', 'jobs': Jobs})

@login_required
def dataset_details(request, data_set_id):
    username = request.user.username
    data_sets_from_fs = user_views.get_data_sets_from_fs(username)
    data_set_url = data_sets_from_fs[data_set_id][0]
    data_set_files = data_sets_from_fs[data_set_id][1]
    data_set_file_details = []
    for data_set_file in data_set_files:
        file_path = data_set_url + "/" + data_set_file
        file_uploaded_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(os.path.getmtime(file_path)))
        current_file_detail = dict()
        current_file_detail['name'] = data_set_file
        current_file_detail['uploaded_time'] = file_uploaded_time
        current_file_detail['file_path'] = file_path
        data_set_file_details.append(current_file_detail)

    content = {'title': 'MagiEye - Dataset Details', 'data_set_id': data_set_id,
               'data_set_url': data_set_url, 'data_set_file_details': data_set_file_details}

    return render(request, 'dataset_details.html', content)

@login_required
def job_details(request, job_id):
    username = request.user.username
    jobs_from_fs = user_views.get_jobs_from_fs(username)

    job_url = jobs_from_fs[job_id][0]
    job_files = jobs_from_fs[job_id][1]
    job_details = []

    for file_name in job_files:
        file_path = job_url + "/" + file_name
        file_uploaded_time = time.strftime('%Y-%m-%d   %H:%M:%S', time.gmtime(os.path.getmtime(file_path)))
        current_file_detail = dict()
        current_file_detail['name'] = file_name
        current_file_detail['uploaded_time'] = file_uploaded_time
        current_file_detail['file_path'] = file_path
        job_details.append(current_file_detail)

    content = {'title': 'MagiEye - Job Details', 'job_id': job_id,
               'job_url': job_url, 'job_details': job_details}

    return render(request, 'job_details.html', content)


@login_required
def admin_view(request):
    if request.user.is_superuser:
        users = User.objects.all()
        return render(request, 'admin_view.html', {'title': 'MagikEye - Admin View', 'customers': users})
    else:
        return HttpResponse("<h1>You don't have privilege to view this page</h1>")

@login_required
def admin_as_user_login(request, user_name):
    user_to_be_logged_in_as = User.objects.get(username=user_name)

    if request.user.is_superuser:
        login(request, user_to_be_logged_in_as)
        return redirect("home")

    else:
        return redirect("login")

