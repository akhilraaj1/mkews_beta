#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    MagikEye URL Definition's For APIs Of Datasets and Jobs
"""
__author__ = "Jigar Patel" 
__e_mail__ = "jigar@magikeye.com" 
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ----------------------------------------------------------------------------------------------------------------------

from django.contrib import admin
from django.urls import path, include
from .router import router
from dbapi import api

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),

    path('api/updateAuthToken/', api.updateAuthToken),

    path('api/getDatasets/', api.getDatasets),
    path('api/getJobs/', api.getJobs),

    path('api/createDataSet/', api.createDataSet),
    path('api/updateDataSet/', api.updateDataSet),
    path('api/removeDataSet/', api.removeDataSet),
    path('api/removeAlldatasets/', api.removeAlldatasets),

    path('api/createJob/', api.createJob),
    path('api/updateJob/', api.updateJob),
    path('api/removeJob/', api.removeJob),
    path('api/removeAlljobs/', api.removeAlljobs),

    path('api/getFSUsers/', api.getFSUsers),
    path('api/createFSUser/', api.createFSUser),

    path('api/createFSDataset/', api.createFSDataset),
    path('api/getFSDatasets/', api.getFSDatasets),
    path('api/addFileToFSDataset/', api.addFileToFSDataset),
    path('api/listFSDatasetFiles/', api.listFSDatasetFiles),
    path('api/getFileFromFSDataset/', api.getFileFromFSDataset),
    path('api/removeFSDataset/', api.removeFSDataset),

    path('api/createFSJob/', api.createFSJob),
    path('api/getFSJobs/', api.getFSJobs),
    path('api/getFileFromFSJob/', api.getFileFromFSJob),
    path('api/removeFSJob/', api.removeFSJob),

    path('api/deleteFSUser/', api.deleteFSUser),

    path('api/jobExecutorQueryJob/', api.jobExecutorQueryJob),


]
