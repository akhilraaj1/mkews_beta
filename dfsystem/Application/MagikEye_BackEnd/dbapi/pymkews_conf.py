#File System Storage and PYMKEWS configuration
import mkews
from mkews.pymkews_controller import pymkews_controller
import os

exec = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                    '..', '..', 'pymkews_worker', 'pymkews_worker', 'pymkews_worker.slurm')

if not os.path.isfile(exec):
    exec = 'pymkews_worker'

controller_config = {
                        'data_storage': {
                            'type': 'osfs',
                            'params': {
                                'root': ''
                            }
                        },
                        'job_executor': {
                            'type': 'slurm',
                            'params': {
                                'execution_rate': 0.2,
                                'failure_rate': 0.5
                            }
                        }
                    }

# DataStorage


data_storage_root = ('/'.join((os.path.abspath(__file__)).split('/')[:-2])) + '/filesystem'
controller_config['data_storage']['params']['root'] = data_storage_root
print(f"*****    Created data storage root: {data_storage_root}    *****")

data_storage = pymkews_controller.DataStorageFactory.create(controller_config['data_storage'])
job_executor = pymkews_controller.JobExecutorFactory.create(controller_config['job_executor'], data_storage)
