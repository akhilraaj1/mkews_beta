# -*- coding: utf-8 -*-

"""
MkEWS Data Storage
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from .data_storage import DataStorage
from .factory import DataStorageFactory

