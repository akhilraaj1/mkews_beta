# -*- coding: utf-8 -*-

"""
MkEWS Worker
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

import logging
import os
from typing import List
import pymkecc

from .job import Job
from pymkews_worker.error import Error
from pymkews_worker.data_storage import DataStorage


# ------------------------------------------------------------------------------

class MkeccV10Job(Job):

    def __init__(self):
        self.logger = logging.getLogger('pymkews.worker.mkeccv10')

    # -------------------------------------------------------------------------

    @staticmethod
    def __update_macros(job_json, job_path, dts_path):
        if 'macros' in job_json['params']:
            macros = job_json['params']['macros']

            if not isinstance(macros, dict):
                raise Error(f'Property "macros" must be an object: {macros}')
        else:
            macros = {}

        macros['DATA_PATH'] = dts_path
        macros['MKEDET_PATH'] = job_path

    # -------------------------------------------------------------------------

    def execute(self, job_json: dict, data_storage: DataStorage) -> List[str]:
        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Executing job ...')

        # Might involve data copying, if not on a shared filesystem
        job_path = data_storage.get_job_path()
        dts_path = data_storage.get_dts_path()

        self.__update_macros(job_json, job_path, dts_path)

        params = job_json['params']
        if 'template' not in params:
            raise Error(f'Job type "{job_json["type"]}" requires "template" parameter')

        if 'dsession' not in params:
            raise Error(f'Job type "{job_json["type"]}" requires "dsession" parameter')

        # Locate template
        template_path = pymkecc.GetTemplatePath(params['template'])
        if not os.path.isfile(template_path):
            raise Error(f'Cannot locate template for "{params["template"]}')

        # Setup session based on template
        self.logger.info(f'Loading template: {template_path}')
        template = pymkecc.SessionTemplate()
        template.setFromFile(template_path)
        self.logger.info('Instantiating template')
        session = template.getInstance(params['macros'])

        # Copy datasets
        dsession = pymkecc.Session()
        dsession.load(os.path.join(dts_path, params['dsession']))
        dts = dsession.getDatasets()
        session.setDatasets(dts)

        # Execute scenario
        session.executeAll()

        # Return a list of result files
        job_files = [f for f in os.listdir(job_path) if f.lower().endswith('.bin')]

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'done')

        return job_files
