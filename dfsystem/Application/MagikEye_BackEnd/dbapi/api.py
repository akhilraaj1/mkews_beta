#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    MagikEye APi's For Handling Datasets and Jobs in DataBase
"""
__author__ = "karthik"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
# ----------------------------------------------------------------------------------------------------------------------

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from django.http import FileResponse
from .models import Datasetsdata, Jobsdata
from .models import AuthToken

import json
import requests
from . import pymkews_conf as PYMKEWS

"""
 SECRET_KEY is same for both the Browser-Interface App and Backend App.
 SECRET_KEY enables a secure communication between Browser-Interface App and Backend App.
"""

from magikeye import settings
APP_SECRET_KEY = settings.SECRET_KEY

# API's communicate with DB:
def validate_secretToken(payload):
    """
        :Author:  karthik
        :Description: Function to validate the secret_token from the provided payload
        :param payload: Dict ['secret_token': str, **kwargs]
        :return: [ response message : str, response code : HTTP.status_code ]
    """
    if "secret_token" not in payload:
        response = {"message": "Please provide the 'secret_token' in payload", "type": "error"}
        return response, status.HTTP_400_BAD_REQUEST
    else:
        validation = False
        if AuthToken.objects.filter(secret_token=payload["secret_token"]):
            if "username" in payload:
                if payload["username"] == AuthToken.objects.get(secret_token=payload["secret_token"]).username:
                    validation = True
            else:
                validation = True
        if validation:
            response = {"message": "secret_token validated successfully!", "type": "success"}
            return response, status.HTTP_200_OK
        else:
            response = {"message": "Invalid secret_token' ", "type": "error"}
            return response, status.HTTP_401_UNAUTHORIZED


@api_view(["POST"])
def updateAuthToken(payload):
    """
    :Author:  karthik
    :Description: API to update[create/delete] secretToken into AuthToken Table for a given user
    :param payload: Json ['username': str, 'secret_token': str, 'secret_key': str, 'action': str]
    :return: Response object
    """
    data = json.loads(payload.body)
    try:
        username = data["username"]
        action = data["action"]
        if action.lower().strip() == "create":
            secret_token = data["token"]

        if data["secret_key"] == APP_SECRET_KEY:
            if not AuthToken.objects.filter(username=username):
                if action.lower().strip() == "create":
                    AuthToken.objects.create(username=username, secret_token=secret_token)
                    response = {"message": f"secret_token has been updated for the user {username}", "type": "success"}
            else:
                if action.lower().strip() == "create":
                    AuthToken.objects.get(username=username).update(secret_token=secret_token)
                    response = {"message": f"secret_token has been updated for the user {username}", "type": "success"}
                if action.lower().strip() == "delete":
                    AuthToken.objects.get(username=username).delete()
                    response = {"message": f"secret_token has been deleted for the user {username}", "type": "success"}

            return Response(response, status.HTTP_200_OK)

        else:
            err_response = {"message": "Invalid secret_key.", "type": "error"}
            return Response(err_response, status.HTTP_401_UNAUTHORIZED)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("token" not in data) or ("secret_key" not in data):
            err_response = {"message": "Please provide the required Field 'username'", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getDatasets(payload):
    """
    :Author:  karthik
    :Description: API to get all datasets for a given user
    :param payload: Json ['username': str, 'secret_token': str]
    :return: Response object
    """
    data = json.loads(payload.body)
    try:
        username = data["username"]
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        datasets = Datasetsdata.objects.filter(dataset_user=username)

        Datasets = []
        for dataset_obj in datasets:
            dataset = dict()
            dataset["id"] = dataset_obj.id
            dataset["dataset_user"] = dataset_obj.dataset_user
            dataset["dataset_date_created"] = dataset_obj.dataset_date_created
            dataset["dataset_date_comment"] = dataset_obj.dataset_date_comment
            dataset["data_set_files"] = PYMKEWS.data_storage.list_dataset_files(dataset_obj.dataset_user, dataset_obj.id)
            Datasets.append(dataset)
        response = {'Datasets': Datasets, "type": "success"}

        return Response(response, status.HTTP_200_OK)

    except Exception as err_message:
        err_message = str(err_message)
        if "username" not in data:
            err_message = {"message": "Please provide the required Field 'username'", "type": "error"}

        return Response(err_message, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def createDataSet(payload):
    """
    :Author:  karthik
    :Description: API to create a dataset for a given user
    :param payload: Json ['username': str, 'secret_token': str]
    :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        ds = {
                'username': data["username"],
                'comment': data.get("comment", "None, Can be updated with the chat icon here!")
             }

        current_data_set = Datasetsdata.objects.create(dataset_user=ds['username'], dataset_date_comment=ds['comment'])
        current_data_set.save()

        data_set_id = Datasetsdata.objects.filter(dataset_user=ds['username']).last().id
        data.update({'data_set_id': data_set_id, 'type': 'success'})

        return Response(data, status.HTTP_200_OK)

    except Exception as err_message:
        err_message = str(err_message)
        if "username" not in data:
            err_message = {"message": "Please provide the required Field 'username'", "type": "error"}

        return Response(err_message, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def updateDataSet(payload):
    """
    :Author:  karthik
    :Description: API to update dataset's comment for a given user
    :param payload: Json ['data_set_id': str, 'comment': str, 'secret_token': str]
    :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        dataset_id = data["data_set_id"]
        comment = data["comment"]

        current_data_set = Datasetsdata.objects.filter(id=dataset_id)
        current_data_set.update(dataset_date_comment=comment)

        data.update({'type': 'success'})
        return JsonResponse(data, safe=False)

    except Exception as err_message:
        err_message = str(err_message)
        if ("data_set_id" not in data) or ("comment" not in data):
            err_message = {"message": "Please provide the required Fields ['data_set_id', 'comment']", "type": "error"}

        return Response(err_message, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def removeDataSet(payload):
    """
    :Author:  karthik
    :Description: API to remove a dataset for a given user
    :param payload: Json ['data_set_id': str, 'secret_token': str]
    :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        dataset_id = data["data_set_id"]
        current_data_set = Datasetsdata.objects.filter(id=dataset_id)
        current_data_set.delete()

        data.update({'type': 'success'})
        return Response(data, status.HTTP_200_OK)

    except Exception as err_message:
        err_message = str(err_message)
        if "data_set_id" not in data:
            err_message = {"message": "Please provide the required Fields 'data_set_id'", "type": "error"}

        return Response(err_message, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def removeAlldatasets(payload):
    """
    :Author:  karthik
    :Description: API to remove all datasets for a given user, Will be called on user deletion.
    :param payload: Json ['username': str, 'secret_key': str]
    :return: Response object
    """
    data = json.loads(payload.body)
    try:
        if "secret_key" in data:
            if data["secret_key"] == APP_SECRET_KEY:
                username = data["username"]
                all_data_sets_of_user = Datasetsdata.objects.filter(dataset_user=username)
                all_data_sets_of_user.delete()

                data.update({'message': f'All Data sets of {username} has been deleted successfully', 'type': 'success'})
                return Response(data, status.HTTP_200_OK)

            else:
                err_response = {"message": "Invalid secret_key.", "type": "error"}
                return Response(err_response, status.HTTP_401_UNAUTHORIZED)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("secret_key" not in data):
            err_response = {"message": "Please provide the Required Fields ['username', 'secret_key']", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getJobs(payload):
    """
        :Author:  karthik
        :Description: API to get all Jobs for a given user
        :param payload: Json ['username': str, 'secret_token': str]
        :return: Response object
        """
    data = json.loads(payload.body)
    try:
        username = data["username"]
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        jobs = Jobsdata.objects.filter(jobs_user=username)
        Jobs = []
        for job_obj in jobs:
            job = dict()
            job["id"] = job_obj.id
            job["jobs_user"] = job_obj.jobs_user
            job["jobs_dataset_id"] = job_obj.jobs_dataset_id
            job["jobs_date_created"] = job_obj.jobs_date_created
            job["jobs_type"] = job_obj.jobs_type
            job["jobs_process"] = job_obj.jobs_process
            Jobs.append(job)
        response = {'Jobs': Jobs, "type": "success"}

        return Response(response, status.HTTP_200_OK)

    except Exception as err_message:
        err_message = str(err_message)
        if "username" not in data:
            err_message = {"message": "Please provide the required Field 'username'",
                           "type": "error"}

        return Response(err_message, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def createJob(payload):
    """
    :Author:  karthik
    :Description: API to create a job for a given user
    :param payload: Json ['username': str, 'dataset_id': str, 'jobs_type': str, 'jobs_process': str,'secret_token': str]
    :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        job = {
                "username": data["username"],
                "dataset_id": data["dataset_id"],
                "type": data["jobs_type"],
                "process": data["jobs_process"]
              }

        current_job = Jobsdata.objects.create(jobs_user=job["username"], jobs_type=job["type"],
                                              jobs_process=job["process"], jobs_dataset_id=job["dataset_id"])
        current_job.save()

        job_id = Jobsdata.objects.filter(jobs_user=job['username']).last().id
        data.update({'job_id': job_id, 'type': 'success'})

        return Response(data, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("dataset_id" not in data) or ("jobs_type" not in data) or ("jobs_process" not in data):
            err_response = {"message": "Please provide the Required Fields\
                           ['username', 'dataset_id', 'jobs_type', 'jobs_process']", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def updateJob(payload):
    """
    :Author:  karthik
    :Description: API to update jobs's process status comment for a given job id
    :param payload: Json ['job_id': str, 'jobs_process': str, 'secret_token': str]
    :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        job = {
                "job_id": data["job_id"],
                "process": data["jobs_process"]
              }

        current_job = Jobsdata.objects.filter(id=job["job_id"])
        current_job.update(jobs_process=job["process"])

        data.update({'type': 'Success'})
        return Response(data, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("job_id" not in data) or ("jobs_process" not in data):
            err_response = {"message": "Please provide the Required Fields ['job_id', 'jobs_process']", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def removeJob(payload):
    """
        :Author:  karthik
        :Description: API to remove a job for a given job id
        :param payload: Json ['job_id': str, 'secret_token': str]
        :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        job_id = data["job_id"]
        current_job = Jobsdata.objects.filter(id=job_id)
        current_job.delete()

        data.update({'type': 'success'})
        return Response(data, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if "job_id" not in data:
            err_response = {"message": "Please provide the Required Field 'job_id'", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def removeAlljobs(payload):
    """
        :Author:  karthik
        :Description: API to remove all jobs for a given user, Will be called on user deletion.
        :param payload: Json ['username': str, 'secret_key': str]
        :return: Response object
    """
    data = json.loads(payload.body)
    try:
        if "secret_key" in data:
            if data["secret_key"] == APP_SECRET_KEY:
                username = data["username"]
                all_jobs_of_user = Jobsdata.objects.filter(jobs_user=username)
                all_jobs_of_user.delete()

                data.update({'message': f'All Jobs of {username} has been deleted successfully', 'type': 'success'})
                return JsonResponse(data, safe=False)
            else:
                err_response = {"message": "Invalid secret_key.", "type": "error"}
                return Response(err_response, status.HTTP_401_UNAUTHORIZED)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("secret_key" not in data):
            err_response = {"message": "Please provide the Required Fields ['username', 'secret_key']", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)



# API's communicate with filesystem:

@api_view(["POST"])
def getFSUsers(payload):
    """
        :Author:  karthik
        :Description: API to get the list of users registered in the FileSystem.
        :return: Response object
    """
    data = json.loads(payload.body)
    try:
        if "secret_key" in data:
            if data["secret_key"] == APP_SECRET_KEY:
                users = PYMKEWS.data_storage.list_users()
                users = [users] if not isinstance(users, list) else users
                response = {"users": users}
                return Response(response, status.HTTP_200_OK)
        else:
            err_response = {"message": "Invalid secret_key.", "type": "error"}
            return Response(err_response, status.HTTP_401_UNAUTHORIZED)

    except Exception as err_response:
        err_response = str(err_response)
        if 'secret_key' not in data:
            err_response = {"message": "Please provide the Required Field ['username', 'secret_key']", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def createFSUser(payload):
    """
        :Author:  karthik
        :Description: API to register a new User in the FileSystem.
        :params: Json ['username': str, 'secret_token': str]
        :return: Response object
    """
    data = json.loads(payload.body)
    try:
        if "secret_key" in data:
            if data["secret_key"] == APP_SECRET_KEY:
                username = data["username"]
                if username not in PYMKEWS.data_storage.list_users():
                    PYMKEWS.data_storage.create_user(username)
                    response = {"message": f"User '{username}' has been added in the filesystem", "type": "success"}
                else:
                    response = {"message": f"User '{username}' has been already added in the filesystem", "type": "success"}
                return Response(response, status.HTTP_200_OK)
        else:
            err_response = {"message": "Invalid secret_key.", "type": "error"}
            return Response(err_response, status.HTTP_401_UNAUTHORIZED)


    except Exception as err_response:
        err_response = str(err_response)
        if ('username' not in data) or ('secret_key' not in data):
            err_response = {"message": "Please provide the Required Field ['username', 'secret_key']", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def createFSDataset(payload):
    """
        :Author:  karthik
        :Description: API to create a new dataset for a given User in the FileSystem.
        :params: Json ['username': str, 'data_set_id': str, 'secret_token': str]
        :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        data_set_id = data["data_set_id"]
        if int(data_set_id) not in PYMKEWS.data_storage.list_datasets(username):
            PYMKEWS.data_storage.create_dataset(username, data_set_id)
            response = {"message": f"Dataset '{data_set_id}' has been created for the user '{username}'", "type": "success"}
        else:
            response = {"message": f"Dataset '{data_set_id}' has been already created for the user '{username}'.",
                        "type": "success"}

        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("data_set_id" not in data):
            err_response = {"message": "Please provide Required Field 'username' and 'data_set_id'",
                            "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getFSDatasets(payload):
    """
        :Author:  karthik
        :Description: API to get the list of datasets for a given User in the FileSystem.
        :params: Json ['username': str, 'secret_token': str]
        :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        fs_data_set_ids = PYMKEWS.data_storage.list_datasets(username)
        fs_data_set_ids = [fs_data_set_ids] if not isinstance(fs_data_set_ids, list) else fs_data_set_ids
        datasets = dict()
        for data_set_id in fs_data_set_ids:
            dataset_url = PYMKEWS.data_storage.get_dataset_url(username, data_set_id)
            dataset_files = PYMKEWS.data_storage.list_dataset_files(username, data_set_id)
            datasets[str(data_set_id)] = (dataset_url, dataset_files)

        response = {"datasets": datasets, "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if 'username' not in data:
            err_response = {"message": "Please provide the Required Field 'username'", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def addFileToFSDataset(payload):
    """
        :Author:  karthik
        :Description: API to add the file into the specified dataset for a given User in the FileSystem.
        :params: Dict ['username': str, 'data_set_id': str, 'filename': str, 'file': file_object,  secret_token': str]
        :return: Response object
    """
    data = payload.data
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        data_set_id = data["data_set_id"]
        file_name = data["filename"]
        file_content = data["file"]

        with open(f"{file_name}", "wb") as f:
            f.write(file_content.read())
            f.flush()
        PYMKEWS.data_storage.add_file_to_dataset(username, data_set_id, f.name)

        response = {"message": f"File {f.name} added to the data set {data_set_id}", "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("data_set_id" not in data) or ("filename" not in data) or ("file" not in data):
            err_response = {"message": "Please provide Required Fields\
                            ['username', 'data_set_id', 'filename', 'file']", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def listFSDatasetFiles(payload):
    """
        :Author:  karthik
        :Description: API to get the list of datasets Files of a specified data_set_id for a given User in the FileSystem.
        :params: Json ['username': str, 'data_set_id': str, 'secret_token': str]
        :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        data_set_id = data["data_set_id"]
        data_set_files = PYMKEWS.data_storage.list_dataset_files(username, data_set_id)
        data_set_files = [data_set_files] if not isinstance(data_set_files, list) else data_set_files

        response = {"data_set_files": data_set_files, "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("data_set_id" not in data):
            err_response = {"message": "Please provide Required Fields ['username', 'data_set_id']", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getFileFromFSDataset(payload):
    """
        :Author:  karthik
        :Description: API to get the file object of a specified data_set_id-file_name for a given User in the FileSystem.
        :params: Json ['username': str, 'data_set_id': str, 'file_name': str, 'secret_token': str]
        :return: FileResponse object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        data_set_id = data["data_set_id"]
        file_name = data["file_name"]
        file_content = PYMKEWS.data_storage.get_file_from_dataset(username, data_set_id, file_name)
        return FileResponse(file_content, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("data_set_id" not in data) or ("file_name" not in data):
            err_response = {"message": "Please provide the  Required Fields\
                            ['username', 'data_set_id', 'file_name']", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def removeFSDataset(payload):
    """
         :Author:  karthik
         :Description: API to remove the specified data_set_id for a given User in the FileSystem.
         :params: Json ['username': str, 'data_set_id': str, 'secret_token': str]
         :return: Response object
     """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        data_set_id = data["data_set_id"]
        if int(data_set_id) in PYMKEWS.data_storage.list_datasets(username):
            PYMKEWS.data_storage.delete_dataset(username, data_set_id)
            response = {"message": f"Dataset '{data_set_id}' has been deleted for the user '{username}'",
                        "type": "success"}
        else:
            response = {"message": f"Dataset '{data_set_id}' has been already deleted for the user '{username}'.",
                        "type": "success"}

        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("data_set_id" not in data):
            err_response = {"message": "Please provide mandatory Fields ['username', 'data_set_id'] in payload",
                            "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def createFSJob(payload):
    """
         :Author:  karthik
         :Description: API to create the specified job_id for a given User in the FileSystem.
         :params: Json ['username': str, 'job_id': str, 'job_file_as_dict': dict, 'secret_token': str]
         :return: Response object
     """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        job_id = data["job_id"]
        job_file_as_dict = data["job_file_as_dict"]

        # Job File Validation - [Job file content validation]
        job_file_validated_successfully = True
        try:
            job_type, job_json = PYMKEWS.job_executor.validate_job(username, job_json=job_file_as_dict)
        except Exception as msg:
            job_file_validated_successfully = False
            if "dataset url does not exist" in str(msg).lower():
                dataset_url = '/filesystem' + str(msg).split('filesystem')[1]
                msg = "Dataset URL does not exist:  " + dataset_url
            err_message = f"Cannot validate job JSON: {str(msg)}"

        pstatus = PYMKEWS.job_executor.PROCESS_UNKNOWN

        if job_file_validated_successfully:
            # Execute job [#create job in data_storage is taken care by the job_executor]
            PYMKEWS.job_executor.execute_job(username, job_id, job_file_as_dict)

            while pstatus >= 0:
                pstatus, results = PYMKEWS.job_executor.query_job(username, job_id)

            if pstatus == PYMKEWS.job_executor.PROCESS_SUCCESS:
                pstatus = f"Process successful"
                PYMKEWS.data_storage.add_data_to_job(username, job_id, 'result.json',
                                                     json.dumps(results, indent=2).encode('utf-8'))
            elif pstatus == PYMKEWS.job_executor.PROCESS_FAILURE:
                pstatus = f"Process failure"
                PYMKEWS.data_storage.add_data_to_job(username, job_id, 'result.json',
                                                     json.dumps(results, indent=2).encode('utf-8'))
            else:
                pstatus = f"Process running"

            response = {"pstatus": pstatus, "type": "success"}
            return Response(response, status.HTTP_200_OK)

        else:
            err_response = {"validationError": 1, "message": err_message, "type": "error"}
            return Response(err_response, status.HTTP_406_NOT_ACCEPTABLE)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("job_id" not in data) or ("job_file_as_dict" not in data):
            err_response = {"message": "Please provide all mandatory fields ['username', 'job_id, 'job_file_as_dict']\
                                  in payload", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getFSJobs(payload):
    """
         :Author:  karthik
         :Description: API to get the jobs for a given User in the FileSystem.
         :params: Json ['username': str, 'secret_token': str]
         :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        jobs = dict()
        jobs_list = PYMKEWS.data_storage.list_jobs(username)
        for job_id in jobs_list:
            job_url = PYMKEWS.data_storage.get_job_url(username, job_id)
            job_files = PYMKEWS.data_storage.list_job_files(username, job_id)
            job_files = [job_files] if not isinstance(job_files, list) else job_files
            jobs[str(job_id)] = (job_url, job_files)

        response = {"jobs": jobs, "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        if 'username' not in data:
            err_response = {"message": "Please provide the Required Field 'username'", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def getFileFromFSJob(payload):
    """
        :Author:  karthik
        :Description: API to get the file object of a specified job_id-file_name for a given User in the FileSystem.
        :params: Json ['username': str, 'job_id': str, 'file_name': str, 'secret_token': str]
        :return: FileResponse object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        job_id = data["job_id"]
        file_name = data["file_name"]
        job_file_content = PYMKEWS.data_storage.get_file_from_job(username, job_id, file_name)
        return FileResponse(job_file_content, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("job_id" not in data) or ("file_name" not in data):
            err_response = {"message": "Please provide the Fields ['username', 'job_id', 'file_name'] in payload",\
                            "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def removeFSJob(payload):
    """
         :Author:  karthik
         :Description: API to remove the job_id for a given User in the FileSystem.
         :params: Json ['username': str, 'job_id': str, 'secret_token': str]
         :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        job_id = data["job_id"]
        PYMKEWS.data_storage.delete_job(username, job_id)
        response = {"message": f"Job-{job_id} has been removed from FileSystem successfully!", "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("job_id" not in data):
            err_response = {"message": "Please provide the Required Fields ['username', 'job_id']", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def deleteFSUser(payload):
    """
        :Author:  karthik
        :Description: API to remove the specified user from the Filesystem, Will be called on user deletion.
        :param payload: Json ['username': str, 'secret_key': str]
        :return: Response object
    """
    data = json.loads(payload.body)
    try:
        if "secret_key" in data:
            if data["secret_key"] == APP_SECRET_KEY:
                username = data["username"]
                if username in PYMKEWS.data_storage.list_users():
                    PYMKEWS.data_storage.delete_user(username)
                    response = {"message": f"User '{username}' has been removed from the filesystem", "type": "success"}
                else:
                    response = {"message": f"User '{username}' had been already removed from the filesystem", "type": "success"}
                return Response(response, status.HTTP_200_OK)

            else:
                err_response = {"message": "Invalid secret_key.", "type": "error"}
                return Response(err_response, status.HTTP_401_UNAUTHORIZED)

    except Exception as err_response:
        err_response = str(err_response)
        if ('username' not in data) or ('secret_key' not in data):
            err_response = {"message": "Please provide the Required Field ['username', 'secret_key']", "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def jobExecutorQueryJob(payload):
    """
         :Author:  karthik
         :Description: API to query the status of a specified process id of job for a given User in the FileSystem.
         :params: Json ['username': str, 'job_id': str, 'pid': str, 'secret_token': str]
         :return: Response object
    """
    data = json.loads(payload.body)
    try:
        response, response_code = validate_secretToken(data)
        if response["type"] == "error":
            return Response(response, response_code)

        username = data["username"]
        job_id = data["job_id"]
        process_status, results = PYMKEWS.job_executor.query_job(username, job_id)
        if process_status == PYMKEWS.job_executor.PROCESS_SUCCESS:
            process_status = f"Process successful"
            PYMKEWS.data_storage.add_data_to_job(username, job_id, 'result.json',
                                                 json.dumps(results, indent=2).encode('utf-8'))
        elif process_status == PYMKEWS.job_executor.PROCESS_FAILURE:
            process_status = f"Process failure"
            PYMKEWS.data_storage.add_data_to_job(username, job_id, 'result.json',
                                                 json.dumps(results, indent=2).encode('utf-8'))
        else:
            process_status = f"Process running"

        response = {"process_status": process_status, "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        err_response = str(err_response)
        if ("username" not in data) or ("job_id" not in data) or ('pid' not in data):
            err_response = {"message": "Please provide the Required Fields ['username', 'job_id', 'pid']",
                            "type": "error"}

        return Response(err_response, status.HTTP_400_BAD_REQUEST)