#!/bin/sh

set -e
gunicorn --chdir /mkews_web/mkews/Application/MagikEye_WebInterface/ connect.wsgi:application --workers=12 --threads=4 --worker-class=gthread --worker-connections=1024 --timeout=300 --keep-alive 5 --access-logfile '/mkews_web/webapp_access_log' --bind :8000 & sh ./startdbapi.sh
